module UsingEuterpea.Lick where

import Euterpea

lick :: Music Pitch
lick = line [
        e 5 en,
        fs 5 en,
        g 5 en,
        a 5 en,

        fs 5 qn,
        d 5 en,
        e 5 den
        ]

playLick :: IO ()
playLick = play lick

module UsingEuterpea.Sine where

import Euterpea
import Data.Audio
import Data.Array.Unboxed
import GHC.Real
import qualified Data.ByteString.Lazy as B
--import Graphics.UI.SDL.Mixer as Mix

type Signal = [Double]
type Hz = Int

sampleRateHz :: Hz
sampleRateHz = 44100

sine :: [Double]
sine = map sin [1.0..4800.0]

sample :: [Double] -> Audio Double
sample dat = Audio { 
                    sampleRate = sampleRateHz,
                    channelNumber = 1,
                    sampleData = listArray (0, length dat - 1) dat
                    }


playSound :: B.ByteString -> IO ()
playSound content = do return ()
        --dir <- getTemporaryDirectory
        --(tmp, h) <- openTempFile dir "sdl-input"
        --B.hPutStr h content
        --hClose h

        --mus <- Mix.loadMUS tmp
        --Mix.playMusic mus 1
        --wait

        -- This would double-free the Music, as it is also freed via a
        -- finalizer
        --Mix.freeMusic mus
        --finalizeForeignPtr mus
        --removeFile tmp

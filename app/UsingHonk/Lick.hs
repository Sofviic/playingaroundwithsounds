module UsingHonk.Lick where

import Sound.Honk
import GHC.Real

lick :: [Note]
lick = [
        Note (1 % 8) 659.2551, --E
        Note (1 % 8) 739.9888, --F#
        Note (1 % 8) 783.9909, --G
        Note (1 % 8) 880.0000, --A

        Note (1 % 4) 739.9888, --F#
        Note (1 % 8) 587.3295, --D
        Note (3 % 8) 659.2551  --E
        ]

playLick :: IO ()
playLick = play lick

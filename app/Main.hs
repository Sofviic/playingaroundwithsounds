module Main (main) where

import qualified UsingHonk.Lick
import qualified UsingEuterpea.Lick
{--

bitPerSample :: Int
bitPerSample = 8

bytesPerSendToSoundCard :: Int
bytesPerSendToSoundCard = 512

soundVolume :: Float
soundVolume = 50

signal :: Signal
signal =  map (220+440*sin) [1.0..48000.0]

sample :: Float -> Signal -> [Note]
sample _ = map (Note $ 1 / sampleRate)
--}

main :: IO ()
main = UsingEuterpea.Lick.playLick


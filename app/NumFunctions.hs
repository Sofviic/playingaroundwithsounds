module NumFunctions() where

instance Num a => Num (b -> a) where
    (+) = liftFunc2 (+)
    (-) = liftFunc2 (-)
    (*) = liftFunc2 (*)
    abs = liftFunc abs
    signum = liftFunc signum
    fromInteger = const . fromInteger

liftFunc2 :: (a -> a -> c) -> (b -> a) -> (b -> a) -> b -> c
liftFunc2 op f g x = f x `op` g x

liftFunc :: (a -> c) -> (b -> a) -> b -> c
liftFunc = (.)

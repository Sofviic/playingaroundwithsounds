# PlayingAroundWithSounds

A place for me to play around with sound/music/audio-related stuff in haskell.

*Note*: I'm mainly using windows at the moment (sometimes arch linux); 
so you may see some windows-specific stuff in here.

At the moment, haven't added a licence, so it's proprietary.
